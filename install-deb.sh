#!/bin/bash
####################################################
#Installation of DVWatcher service for Debian family
# Author: Dmytro Vlasenko
####################################################
echo "#### DVWatcher installer for Debian ####" >&2
echo "Installation in progress ... " >&2

dvwatcher passwd $1 > /dev/null 2&>1
if [ $? -ne 0 ]; then
useradd dvwatcher
fi
if [[ ! -e /etc/dvwatcher ]]; then
mkdir /etc/dvwatcher
fi
chown -R dvwatcher:dvwatcher  /etc/dvwatcher
cp ./config /etc/dvwatcher/conf.cfg
if [[ ! -e /var/lib/dvwatcher ]]; then
mkdir /var/lib/dvwatcher
fi
chown -R dvwatcher:dvwatcher /var/lib/dvwatcher
cp ./dvWatcher.sh /var/lib/dvwatcher/dvWatcher.sh
chmod +x /var/lib/dvwatcher/dvWatcher.sh
cp ./dvWatcher-init-deb /etc/init.d/dvwatcher
chmod +x /etc/init.d/dvwatcher
cd /etc/init.d/
update-rc.d dvwatcher defaults 97 03
mkdir /var/log/dvwatcher/
chown -R dvwatcher:dvwatcher /var/log/dvwatcher/
echo "#### Finished ####" >&2

# if ok - finished if errors - finsih with errors
