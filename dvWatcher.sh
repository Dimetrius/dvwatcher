#!/bin/bash
####################################################
# DVWatcher keep alive and monitor service
# Author: Dmytro Vlasenko
####################################################
CONF_CHK1=`wc -c 2>/dev/null < /etc/dvwatcher/conf.cfg`
CONF_CHK2=`wc -l 2>/dev/null < /etc/dvwatcher/conf.cfg`

echo "Validating config...." >&2
if [[ "$CONF_CHK1" -ge "59"  &&  "$CONF_CHK2" -eq "4" ]] ;
then
 echo "Config ok." >&2
else
echo "Config is empty or have broken structure. Please review /etc/dvwatcher/conf.cfg" >&2
sleep 2
exit
fi

source /etc/dvwatcher/conf.cfg

while true

	do
    stat_check() {
     ps -ef |grep $service_name |grep -v grep | wc -l
    }
#STAT=`ps -ef |grep $service_name |grep -v grep | wc -l`
#if [ $STAT -ne 1 ]; then
if [  `stat_check` -ne 1 ]; then
#echo "Service $service_name is down. Trying to restart..."
echo "Service $service_name is down. Trying to restart..." | mutt -e "my_hdr From: DVWatcher-Notifications <dvwatcher@dvwatcher.local>" -s "Service status alert" $email

  for ((n=0;n<$attempts_number;n++)); do
  #STAT=`ps -ef |grep $service_name |grep -v grep | wc -l`
  #if [ $STAT -ne 1 ]; then
  if [ `stat_check` -ne 1 ]; then
 sudo service $service_name start
# else echo "started after $n attempts"
else echo "started after $n attempts" | mutt -e "my_hdr From: DVWatcher-Notifications <dvwatcher@dvwatcher.local>" -s "Service status alert" $email

fi
 done

fi
#STAT=`ps -ef |grep $service_name |grep -v grep | wc -l`
#if [ $STAT -ne 1 ]; then
if [ `stat_check` -ne 1 ]; then
#  echo "Failed to start after $attempts_number attempts"
echo "Failed to start after $attempts_number attempts" | mutt -e "my_hdr From: DVWatcher-Notifications <dvwatcher@dvwatcher.local>" -s "Service status alert" $email


 #done

fi
    echo " `date` Service $service_name is running" > /var/log/dvwatcher/dvwatcher.log

    sleep $poll_interval

	done
