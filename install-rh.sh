#!/bin/bash
####################################################
#Installation of DVWatcher service for RedHat family
# Author: Dmytro Vlasenko
####################################################
echo "#### DVWatcher installer for RedHat ####" >&2
echo "Installation in progress ... " >&2

dvwatcher passwd $1 > /dev/null 2&>1
if [ $? -ne 0 ]; then
useradd dvwatcher
fi
if [[ ! -e /etc/dvwatcher ]]; then
mkdir /etc/dvwatcher
fi
chown -R dvwatcher:dvwatcher  /etc/dvwatcher
cp ./config /etc/dvwatcher/conf.cfg
if [[ ! -e /var/lib/dvwatcher ]]; then
mkdir /var/lib/dvwatcher
fi
chown -R dvwatcher:dvwatcher /var/lib/dvwatcher
cp ./dvWatcher.sh /var/lib/dvwatcher/dvWatcher.sh
chmod +x /var/lib/dvwatcher/dvWatcher.sh
cp ./dvWatcher-init-rh /etc/init.d/dvwatcher
chmod +x /etc/init.d/dvwatcher
cd /etc/init.d/
chkconfig dvwatcher on
mkdir /var/log/dvwatcher/
chown -R dvwatcher:dvwatcher /var/log/dvwatcher/
echo "#### Finished ####" >&2
